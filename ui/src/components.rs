use yew::prelude::*;

pub struct App;

impl Component for App {
    type Message = ();
    type Properties = ();

    fn create(_props: Self::Properties, _complink: ComponentLink<Self>) -> Self {
        App
    }

    fn update(&mut self, _msg: Self::Message) -> ShouldRender {
        false
    }

    fn change(&mut self, _props: Self::Properties) -> ShouldRender {
        false
    }

    fn view(&self) -> Html {
        html! {
            <>
                <nav class=("navbar", "navbar-expand-md", "fixed-top", "navbar-dark", "bg-dark",
                            "text-light"),
                     role="navigation">
                    <div class="container">
                        <a class=("navbar-brand") href="#">{ "StopTime" }</a>
                        <button class="navbar-toggler",
                                type="button",
                                data-toggle="collapse",
                                data-target="#navbarNav",
                                aria-controls="navbarNav",
                                aria-expanded="false",
                                aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"/>
                        </button>
                    </div>
                </nav>
                <div class="container">
                    <div class="jumbotron">
                        <h1>{ "Hello world!" }</h1>
                    </div>
                </div>
                <footer class=("footer", "bg-dark", "text-light")>
                  <div class=("container", "text-center")>
                      <small>{ "StopTime – © 2019 Paul van Tilburg" }</small>
                  </div>
                </footer>
            </>
        }
    }
}
