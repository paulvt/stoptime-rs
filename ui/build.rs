// Generates static assets.

use std::env;
use std::error::Error;
use std::fs::File;
use std::io::Write;
use std::path::Path;

use sass_rs::{self, Options};

fn main() -> Result<(), Box<dyn Error>> {
    let dir = env::var("CARGO_MANIFEST_DIR").unwrap();
    let scss_dir = Path::new(&dir).join("scss");
    let source_css_file = scss_dir.join("stoptime.scss");
    let css_dir = Path::new(&dir).join("static").join("css");
    let target_css_file = css_dir.join("stoptime.css");

    let sass = sass_rs::compile_file(&source_css_file, Options::default())
        .map_err(|err| format!("Sass compilation failed: {}", err))?;
    let mut f = File::create(&target_css_file).unwrap();
    f.write_all(sass.as_bytes()).unwrap();
    Ok(())
}
