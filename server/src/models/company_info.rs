use chrono::NaiveDateTime;
use serde_derive::{Deserialize, Serialize};

use crate::schema::company_infos;

/// The company (information) model.
///
/// This model represents information about the company or sole proprietorship of the user of
/// StopTime.
#[derive(Associations, Debug, Identifiable, Deserialize, Queryable, Serialize)]
#[belongs_to(CompanyInfo, foreign_key = "original_id")]
#[table_name = "company_infos"]
pub struct CompanyInfo {
    /// The unique identification number
    pub id: i32,
    /// The international bank account number
    pub accountiban: String,
    /// The name of the bank account holder
    pub accountname: String,
    /// The number of the bank account
    pub accountno: String,
    /// The city part of the address
    pub address_city: String,
    /// The postal code part of the address
    pub address_postal_code: String,
    /// The street part of the address
    pub address_street: String,
    /// The bank identification code (or: SWIFT code)
    pub bank_bic: String,
    /// The name of the bank
    pub bank_name: String,
    /// The cellular phone number
    pub cell: String,
    /// The chamber of commerce ID number
    pub chamber: String,
    /// The personal contact name
    pub contact_name: String,
    /// The country of residence
    pub country: String,
    /// The two letter country code
    pub country_code: String,
    /// The email address
    pub email: String,
    /// The official company name
    pub name: String,
    /// The ID of the previous company information model (if any)
    pub original_id: Option<i32>,
    /// The phone number
    pub phone: String,
    /// The VAT number
    pub vatno: String,
    /// The website
    pub website: String,
    /// The time of creation
    pub created_at: NaiveDateTime,
    /// The time of last update
    pub updated_at: NaiveDateTime,
}

/// The new company (information) model.
///
/// This model represents new company information that can be inserted into the database.
#[derive(Debug, Deserialize, Insertable, Serialize)]
#[table_name = "company_infos"]
pub struct NewCompanyInfo {
    /// The international bank account number
    pub accountiban: String,
    /// The name of the bank account holder
    pub accountname: String,
    /// The number of the bank account
    pub accountno: String,
    /// The city part of the address
    pub address_city: String,
    /// The postal code part of the address
    pub address_postal_code: String,
    /// The street part of the address
    pub address_street: String,
    /// The bank identification code (or: SWIFT code)
    pub bank_bic: String,
    /// The name of the bank
    pub bank_name: String,
    /// The cellular phone number
    pub cell: String,
    /// The chamber of commerce ID number
    pub chamber: String,
    /// The personal contact name
    pub contact_name: String,
    /// The country of residence
    pub country: String,
    /// The two letter country code
    pub country_code: String,
    /// The email address
    pub email: String,
    /// The official company name
    pub name: String,
    /// The ID of the previous company information model (if any)
    pub original_id: Option<i32>,
    /// The phone number
    pub phone: String,
    /// The VAT number
    pub vatno: String,
    /// The website
    pub website: String,
}
