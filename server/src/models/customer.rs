use chrono::NaiveDateTime;
use serde_derive::{Deserialize, Serialize};

use crate::schema::customers;

/// The customer model.
///
/// This model represents a customer that has projects/tasks for which invoices need to be
/// generated.
#[derive(AsChangeset, Debug, Deserialize, Identifiable, Queryable, Serialize)]
#[table_name = "customers"]
pub struct Customer {
    /// The unique identification number
    pub id: i32,
    /// The city part of the address
    pub address_city: String,
    /// The postal code part of the address
    pub address_postal_code: String,
    /// The street part of the address
    pub address_street: String,
    /// The email address
    pub email: String,
    /// The name of the financial contact person/department
    pub financial_contact: String,
    /// The default hourly rate (if applicable)
    pub hourly_rate: Option<f32>,
    /// The official (long) name
    pub name: String,
    /// The phone number
    pub phone: String,
    /// The abbreviated name
    pub short_name: String,
    /// Flag whether the customer requires time specificaions
    pub time_specification: bool,
    /// The time of creation
    pub created_at: NaiveDateTime,
    /// The time of last update
    pub updated_at: NaiveDateTime,
}

/// The new customer model
///
/// This model represents a new customer that can be inserted into the database.
#[derive(Default, Deserialize, Insertable, Serialize)]
#[table_name = "customers"]
pub struct NewCustomer {
    /// The city part of the address
    pub address_city: String,
    /// The postal code part of the address
    pub address_postal_code: String,
    /// The street part of the address
    pub address_street: String,
    /// The email address
    pub email: String,
    /// The name of the financial contact person/department
    pub financial_contact: String,
    /// The default hourly rate (if applicable)
    pub hourly_rate: Option<f32>,
    /// The official (long) name
    pub name: String,
    /// The phone number
    pub phone: String,
    /// The abbreviated name
    pub short_name: String,
    /// Flag whether the customer requires time specificaions
    pub time_specification: bool,
}
