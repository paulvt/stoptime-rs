use chrono::NaiveDateTime;
use serde_derive::{Deserialize, Serialize};

use crate::models::{CompanyInfo, Customer};
use crate::schema::invoices;

/// The invoice model.
///
/// This model represents an invoice for a customer that contains billed tasks and through the
/// tasks the registered time.
#[derive(AsChangeset, Associations, Debug, Deserialize, Identifiable, Queryable, Serialize)]
#[belongs_to(CompanyInfo)]
#[belongs_to(Customer)]
#[table_name = "invoices"]
pub struct Invoice {
    /// The unique identification number
    pub id: i32,
    /// The ID of the company info at the time of billing
    pub company_info_id: i32,
    /// The ID of associated customer
    pub customer_id: i32,
    /// Flag whether the invoice should include a time specification
    pub include_specification: bool,
    /// The invoice number
    pub number: i32,
    /// Flag whether the invoice has been paid
    pub paid: bool,
    /// The time of creation
    pub created_at: NaiveDateTime,
    /// The time of last update
    pub updated_at: NaiveDateTime,
}

/// The new invoice model.
///
/// This model represents an new invoice for a customer that can be inserted into the database.
#[derive(Debug, Deserialize, Insertable, Serialize)]
#[table_name = "invoices"]
pub struct NewInvoice {
    /// The ID of the company info at the time of billing
    pub company_info_id: i32,
    /// The ID of associated customer
    pub customer_id: i32,
    /// Flag whether the invoice should include a time specification
    pub include_specification: bool,
    /// The invoice number
    pub number: i32,
    /// Flag whether the invoice has been paid
    pub paid: bool,
}
