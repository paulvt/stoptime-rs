use chrono::NaiveDateTime;
use serde_derive::{Deserialize, Serialize};

use crate::models::Task;
use crate::schema::time_entries;

/// The time entry model.
///
/// This model represents an amount of time that is registered for a certain task.
#[derive(AsChangeset, Associations, Debug, Deserialize, Identifiable, Queryable, Serialize)]
#[belongs_to(Task)]
#[table_name = "time_entries"]
pub struct TimeEntry {
    /// The unique identification number
    pub id: i32,
    /// Flag whether to bill or not
    pub bill: bool,
    /// An additional comment
    pub comment: String,
    /// The finish/end time of the entry
    pub end: NaiveDateTime,
    /// The start time of the entry
    pub start: NaiveDateTime,
    /// The ID of the task the entry is registered for
    pub task_id: i32,
    /// The time of creation
    pub created_at: NaiveDateTime,
    /// The time of last update
    pub updated_at: NaiveDateTime,
}

/// The new time entry model.
///
/// This model represents a new registered amount of time that can be inserted into the database.
#[derive(Debug, Deserialize, Insertable, Serialize)]
#[table_name = "time_entries"]
pub struct NewTimeEntry {
    /// Flag whether to bill or not
    pub bill: bool,
    /// An additional comment
    pub comment: String,
    /// The finish/end time of the entry
    pub end: NaiveDateTime,
    /// The start time of the entry
    pub start: NaiveDateTime,
    /// The ID of the task the entry is registered for
    pub task_id: i32,
}
