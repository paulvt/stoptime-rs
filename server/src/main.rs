#![recursion_limit = "256"]
#![feature(proc_macro_hygiene, decl_macro)]

use rocket::{catchers, routes, Rocket};
use rocket_contrib::serve::StaticFiles;

pub mod catchers;
pub mod handlers;

fn rocket() -> Rocket {
    let static_dir = concat!(env!("CARGO_MANIFEST_DIR"), "/../ui/static");
    let static_files = StaticFiles::from(static_dir);

    rocket::ignite()
        .mount("/", static_files)
        .mount(
            "/company",
            routes![handlers::company::index, handlers::company::create],
        )
        .mount(
            "/customers",
            routes![
                handlers::customers::index,
                handlers::customers::create,
                handlers::customers::new,
                handlers::customers::show,
                handlers::customers::update,
                handlers::customers::destroy,
                handlers::customers::invoices::create,
                handlers::customers::invoices::new,
                handlers::customers::invoices::show,
                handlers::customers::invoices::update,
                handlers::customers::tasks::create,
                handlers::customers::tasks::new,
                handlers::customers::tasks::show,
                handlers::customers::tasks::update,
                handlers::customers::tasks::destroy,
            ],
        )
        .mount("/invoices", routes![handlers::invoices::index])
        .mount(
            "/timeline",
            routes![
                handlers::timeline::index,
                handlers::timeline::create,
                handlers::timeline::new,
                handlers::timeline::show,
                handlers::timeline::update,
                handlers::timeline::destroy,
            ],
        )
        .register(catchers![
            catchers::not_found,
            catchers::unprocessable_entity
        ])
}

fn main() {
    rocket().launch();
}
