//! The root handlers

use rocket::get;

pub mod company;
pub mod customers;
pub mod invoices;
pub mod timeline;

/// Presents the dashboard/overview as start/home view
///
/// It lists the running tasks and projects per customer and shows a global summary.
#[get("/")]
pub fn index() {
    unimplemented!()
}
