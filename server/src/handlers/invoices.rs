//! The invoices handlers
//!
//! The handler is used for showing a list of all invoices.

use rocket::get;

/// Shows a list of invoices, sorted per customer.
#[get("/")]
pub fn index() {
    unimplemented!()
}
