//! The timeline handlers
//!
//! These handlers are used for presenting a timeline of registered time and also for quickly
//! registering new time entries.

use rocket::{delete, get, post, put};

/// Retrieves all registered time entries in descending order to present the timeline.
#[get("/")]
pub fn index() {
    unimplemented!()
}

/// Registers a time entry and redirects back.
#[post("/")]
pub fn create() {
    unimplemented!()
}

/// Shows a form for quickly registering time using a list of customers and tasks and the current
/// date and time.
#[get("/new")]
pub fn new() {
    unimplemented!()
}

/// Show a form of the time entry with the given ID for updating it.
#[get("/<_id>")]
pub fn show(_id: u32) {
    unimplemented!()
}

/// Update the time entry with the given ID.
#[put("/<_id>")]
pub fn update(_id: u32) {
    unimplemented!()
}

/// Destroys the time entry with the given ID.
#[delete("/<_id>")]
pub fn destroy(_id: u32) {
    unimplemented!()
}
