//! The invoice handlers
//!
//! These handlers are for creating and viewing invoices for a specific customer.

use rocket::{get, post, put};

/// Creates a new invoice object for the customer the given ID.
///
/// A unique number is generated for the invoice by taking the year and a sequence number.
///
/// Fixed cost tasks are directly tied to the invoice.
///
/// For a task with an hourly rate, a task copy is created with the select time entries that need
/// to be billed and put in the invoice; the remaining unbilled time entries are left in the
/// original task.
#[post("/<_customer_id>/invoices")]
pub fn create(_customer_id: u32) {
    unimplemented!()
}

/// Generates the form to create a new invoice object by listing unbulled fixed cost tasks and
/// unbilled registered time (for tasks with an hourly rate) of the customer with the given ID so
/// that a selection can be made.
#[get("/<_customer_id>/invoices/new")]
pub fn new(_customer_id: u32) {
    unimplemented!()
}

/// Shows a form for the invoice with the given number for the customer with the given ID
/// and shows a firm for updating it.
// FIXME: Handle PDF and LaTex generation here too!
#[get("/<_customer_id>/invoices/<_number>", rank = 2)] // Number could be "new"
pub fn show(_customer_id: u32, _number: String) {
    unimplemented!()
}

/// Updates the invoices with the given number for the customer with the given ID.
#[put("/<_customer_id>/invoices/<_number>")]
pub fn update(_customer_id: u32, _number: String) {
    unimplemented!()
}
