//! The tasks handlers
//!
//! These handlers are for creating, editing and deleting a task for a specific customer.

use rocket::{delete, get, post, put};

/// Creates a new task oject for a customer with the given ID.
#[post("/<_customer_id>/tasks")]
pub fn create(_customer_id: u32) {
    unimplemented!()
}

/// Provides the form for the data required to create a new task for a customer with the given ID.
#[get("/<_customer_id>/tasks/new")]
pub fn new(_customer_id: u32) {
    unimplemented!()
}

/// Shows a form for viewing and updating information of the task with the given ID for
/// a customer with the given ID.
#[get("/<_customer_id>/tasks/<_id>", rank = 2)] // FIXME: Why is rank 2 necessary?
pub fn show(_customer_id: u32, _id: u32) {
    unimplemented!()
}

/// Updates the task with the given ID for a customer with the given ID.
#[put("/<_customer_id>/tasks/<_id>")]
pub fn update(_customer_id: u32, _id: u32) {
    unimplemented!()
}

/// Destroys the task with the given ID of a customer with the given ID.
#[delete("/<_customer_id>/tasks/<_id>")]
pub fn destroy(_customer_id: u32, _id: u32) {
    unimplemented!()
}
