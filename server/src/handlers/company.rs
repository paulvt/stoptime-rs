//! The company (information) handlers
//!
//! These handlers are for showing and updating information of the company of the user.

use rocket::{get, post};

/// Shows a form with the company information that allows for updating it.
///
/// When updating, it will create a new revision.  The handler allows showing other revisions.
// FIXME: Implement revisions!
#[get("/")]
pub fn index() {
    unimplemented!()
}

/// Updates the company information by creating a new revision.
#[post("/")]
pub fn create() {
    unimplemented!()
}
