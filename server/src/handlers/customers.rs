//! The customer handlers
//!
//!  Handlers for viewing a list of existing customers or creating a new one.

use rocket::{delete, get, post, put};

pub mod invoices;
pub mod tasks;

/// Shows the list of customers.
#[get("/")]
pub fn index() {
    unimplemented!()
}

/// Creates a new customer and redirects to the show handler.
#[post("/")]
pub fn create() {
    unimplemented!()
}

/// Provides the form for the data required to create a new customer.
#[get("/new")]
pub fn new() {
    unimplemented!()
}

/// Shows a form for viewing and updating information of the customer with the given ID.
#[get("/<_id>")]
pub fn show(_id: u32) {
    unimplemented!()
}

/// Updates the customer with the given ID.
#[put("/<_id>")]
pub fn update(_id: u32) {
    unimplemented!()
}

/// Destroys the customer with the given ID and redirects to the index handler.
#[delete("/<_id>")]
pub fn destroy(_id: u32) {
    unimplemented!()
}
