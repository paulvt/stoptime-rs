//! The application error catchers

use rocket::catch;
use rocket_contrib::json;
use rocket_contrib::json::JsonValue;

/// Catches an HTTP 404 (Not Found) error.
#[catch(404)]
pub fn not_found() -> JsonValue {
    json!({
        "status": "error",
        "reason": "Resource was not found",
    })
}

/// Catches an HTTP 422 (Unprocessable Entity) error.
#[catch(422)]
pub fn unprocessable_entity() -> JsonValue {
    json!({
        "status": "error",
        "reason": "Could not parse JSON body or fields were missing",
    })
}
