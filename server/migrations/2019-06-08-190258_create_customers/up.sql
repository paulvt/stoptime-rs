CREATE TABLE customers (
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "address_city" VARCHAR(255) NOT NULL,
  "address_postal_code" VARCHAR(255) NOT NULL,
  "address_street" VARCHAR(255) NOT NULL,
  "email" VARCHAR(255) NOT NULL,
  "financial_contact" VARCHAR(255) NOT NULL,
  "hourly_rate" FLOAT,
  "name" VARCHAR(255) NOT NULL,
  "phone" VARCHAR(255) NOT NULL,
  "short_name" VARCHAR(255) NOT NULL,
  "time_specification" BOOLEAN NOT NULL,
  "created_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  "updated_at" TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP
)
