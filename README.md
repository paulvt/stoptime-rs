# Stop Time

A web application for task/project time registration and invoicing.

This application is written in Rust using the Rocket/Diesel and Yew frameworks.

**WORK IN PROGRESS!** This application is a rewrite of _Stop… Camping Time!_ and
is still very much work in progress.  It will not include the full feature set
of the original application (as described  out below) until version 1.0.0 is
reached

## Features

* Running project & tasks overview
* Timeline overview of registered time
* Management customer information
* Administration of running and billed projects/task with
  * fixed cost, or
  * hourly rates
* Administration of invoices
* Invoice generation in PDF/LaTeX format
  * can include a time specification if required by the customer
* Fully responsive (can be used om smartphone, tablet and desktop)

## Requirements

_Stop Time_ is a Rocket/Yew application. To build it you need:

* Rust nightly (≥ 1.31)
* Cargo

To deploy it you also need:

* Diesel CLI (≥ 1.4.2)

Finally, for invoice generated you need the following LaTeX programs:

* pdflatex, with:
  * isodoc package (>= 1.00)
* rubber

## Installation

For now, _Stop Time_ is in a developing state and not ready for
site-wide deployment yet.  Instructions will follow later.

## Usage

Instructions will be added here later.

## License

_Stop Time_ is free software; you can use, copy, modify, merge, publish,
(re)distribute, sublicense, and/or sell copies of it under the terms of the
MIT License.
